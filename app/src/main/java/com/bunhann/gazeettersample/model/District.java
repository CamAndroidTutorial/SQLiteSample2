package com.bunhann.gazeettersample.model;

public class District {

    private int id;
    private String nameEn;
    private String nameKh;
    private int proCode;

    public District() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameKh() {
        return nameKh;
    }

    public void setNameKh(String nameKh) {
        this.nameKh = nameKh;
    }

    public int getProCode() {
        return proCode;
    }

    public void setProCode(int proCode) {
        this.proCode = proCode;
    }

    @Override
    public String toString() {
        return "ID: " + this.id + " , Name: " + this.nameKh;
    }
}
