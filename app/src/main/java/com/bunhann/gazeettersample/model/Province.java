package com.bunhann.gazeettersample.model;

public class Province {

    private int proCode;
    private int prefix;
    private String province;
    private String provinceKh;

    public Province() {
    }

    public int getProCode() {
        return proCode;
    }

    public void setProCode(int proCode) {
        this.proCode = proCode;
    }

    public int getPrefix() {
        return prefix;
    }

    public void setPrefix(int prefix) {
        this.prefix = prefix;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvinceKh() {
        return provinceKh;
    }

    public void setProvinceKh(String provinceKh) {
        this.provinceKh = provinceKh;
    }
}
