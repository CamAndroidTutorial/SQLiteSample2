package com.bunhann.gazeettersample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bunhann.gazeettersample.R;
import com.bunhann.gazeettersample.model.Province;

import java.util.ArrayList;

public class ProvinceAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Province> provinceList;
    private LayoutInflater inflater;

    public ProvinceAdapter(Context context, ArrayList<Province> list) {
        this.context = context;
        this.provinceList = list;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return provinceList.size();
    }

    @Override
    public Object getItem(int position) {
        return provinceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return provinceList.get(position).getProCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null ){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.spinner_textview, null);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.textView1);
            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Province p = provinceList.get(position);
        viewHolder.textView.setText(p.getProvinceKh());

        return convertView;
    }

    private static class ViewHolder{
        TextView textView;
    }
}
