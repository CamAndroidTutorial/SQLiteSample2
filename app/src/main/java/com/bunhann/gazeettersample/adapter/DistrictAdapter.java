package com.bunhann.gazeettersample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bunhann.gazeettersample.R;
import com.bunhann.gazeettersample.model.District;

import java.util.ArrayList;

public class DistrictAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<District> districtList;
    private LayoutInflater inflater;

    public DistrictAdapter(Context context, ArrayList<District> districtList) {
        this.context = context;
        this.districtList = districtList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return districtList.size();
    }

    @Override
    public Object getItem(int position) {
        return districtList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return districtList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null ){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.spinner_textview, null);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.textView1);
            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        District d = districtList.get(position);
        viewHolder.textView.setText(d.getNameKh());

        return convertView;

    }

    private static class ViewHolder{
        TextView textView;
    }
}
