package com.bunhann.gazeettersample.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bunhann.gazeettersample.R;
import com.bunhann.gazeettersample.model.District;

import java.util.ArrayList;

public class DistrictRecyclerAdapter extends RecyclerView.Adapter<DistrictRecyclerAdapter.ViewHolder> {


    private Context context;
    private ArrayList<District> districts;
    private LayoutInflater inflater;

    public DistrictRecyclerAdapter(Context context, ArrayList<District> districts) {
        this.context = context;
        this.districts = districts;
        this.inflater = LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflate the custom layout
        View convertView = inflater.inflate(R.layout.recycler_textview, parent, false);
        // Return a new holder instance
        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        District district = districts.get(position);
        holder.tvNameEn.setText(district.getNameEn());
        holder.tvNameKh.setText(district.getNameKh());

    }

    @Override
    public int getItemCount() {
        return districts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvNameEn;
        public TextView tvNameKh;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNameEn = (TextView) itemView.findViewById(R.id.tvNameEn);
            tvNameKh = (TextView) itemView.findViewById(R.id.tvNameKh);
        }

    }
}
