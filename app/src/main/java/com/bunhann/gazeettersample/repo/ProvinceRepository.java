package com.bunhann.gazeettersample.repo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.bunhann.gazeettersample.DBHelper;
import com.bunhann.gazeettersample.model.Province;

import java.util.ArrayList;

public class ProvinceRepository {

    private Context context;

    public ProvinceRepository(Context context) {
        this.context = context;
    }

    public Province getProvince(int proCode){

        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String [] sqlSelect = {"PROCODE", "PROVINCE", "PROVINCE_KH"};
        String sqlTables = "provinces";

        qb.setTables(sqlTables);
        Cursor c = qb.query(db, sqlSelect, sqlSelect[0] + "=?", new String[]{String.valueOf(proCode)},
                null, null, null);
        if (c.getCount()>0){
            c.moveToFirst();
            Province p = new Province();
            p.setProCode(c.getInt(0));
            p.setProvince(c.getString(1));
            p.setProvinceKh(c.getString(2));
            c.close();
            return p;
        } else return null;
    }

    public ArrayList<Province> getProvinceList(){

        ArrayList<Province> provinces = new ArrayList<>();
        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();

        String allProvinceSQL = "SELECT * FROM provinces";

        Cursor c = db.rawQuery(allProvinceSQL, null);
        if (c.moveToFirst()){
            do {
                Province province = new Province();
                province.setProCode(c.getInt(c.getColumnIndex("PROCODE")));
                province.setProvince(c.getString(c.getColumnIndex("PROVINCE")));
                province.setProvinceKh(c.getString(c.getColumnIndex("PROVINCE_KH")));
                provinces.add(province);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return provinces;
    }
}
