package com.bunhann.gazeettersample.repo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bunhann.gazeettersample.DBHelper;
import com.bunhann.gazeettersample.model.District;

import java.util.ArrayList;

public class DistrictRepository {

    private Context context;

    public DistrictRepository(Context context) {
        this.context = context;
    }

    public District getDistrict(long districtCode){

        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();

        String sql = "SELECT * FROM districts WHERE DCode=?";
        Cursor c = db.rawQuery(sql, new String[] {String.valueOf(districtCode)});
        if (c!=null) {
            c.moveToFirst();
            District d = new District();
            d.setId(c.getInt(c.getColumnIndex("DCode")));
            d.setNameEn(c.getString(c.getColumnIndex("DName_en")));
            d.setNameKh(c.getString(c.getColumnIndex("DName_kh")));
            c.close();
            db.close();
            return d;
        }
        return null;

    }

    public ArrayList<District> getDistrictByProvince (long provinceCode){

        ArrayList<District> districtList = new ArrayList<>();
        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();

        String sql = "SELECT * FROM districts WHERE PCode=?";
        Cursor c = db.rawQuery(sql, new String[] {String.valueOf(provinceCode)});
        if (c!=null) {
            c.moveToFirst();
            do {
                District d = new District();
                d.setId(c.getInt(c.getColumnIndex("DCode")));
                d.setNameEn(c.getString(c.getColumnIndex("DName_en")));
                d.setNameKh(c.getString(c.getColumnIndex("DName_kh")));
                districtList.add(d);
            } while (c.moveToNext());
            c.close();
            db.close();
            return districtList;
        }
        return null;
    }

}
