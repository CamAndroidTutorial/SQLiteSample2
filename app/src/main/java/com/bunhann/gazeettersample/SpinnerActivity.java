package com.bunhann.gazeettersample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bunhann.gazeettersample.adapter.DistrictAdapter;
import com.bunhann.gazeettersample.adapter.ProvinceAdapter;
import com.bunhann.gazeettersample.model.District;
import com.bunhann.gazeettersample.model.Province;
import com.bunhann.gazeettersample.repo.DistrictRepository;
import com.bunhann.gazeettersample.repo.ProvinceRepository;

import java.util.ArrayList;

public class SpinnerActivity extends AppCompatActivity {

    private Spinner provinceSpinner, districtSpinner;
    private ArrayList<Province> provinces;
    private ArrayList<District> districts;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        provinceSpinner = (Spinner) findViewById(R.id.spinnerProvince);
        districtSpinner = (Spinner) findViewById(R.id.spinnerDistrict);

        ProvinceRepository provinceRepository = new ProvinceRepository(this);
        provinces = new ArrayList<>();

        Province p0 = new Province();
        p0.setProCode(0);
        p0.setProvinceKh("សូមជ្រើសរើសមួយ");
        p0.setProvince("សូមជ្រើសរើសមួយ");
        provinces.add(p0);
        provinces.addAll(provinceRepository.getProvinceList());

        ProvinceAdapter provinceAdapter = new ProvinceAdapter(this, provinces);
        provinceSpinner.setAdapter(provinceAdapter);

        provinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                districts = new ArrayList<>();

                District d0 = new District();
                d0.setId(0);
                d0.setNameKh("សូមជ្រើសរើសមួយ");
                d0.setNameEn("សូមជ្រើសរើសមួយ");
                districts.add(d0);
                if (position==0){
                    districtSpinner.setSelection(-1);
                } else {
                    DistrictRepository districtRepository = new DistrictRepository(view.getContext());
                    districts.addAll(districtRepository.getDistrictByProvince(id));
                    DistrictAdapter districtAdapter = new DistrictAdapter(view.getContext(), districts);
                    districtSpinner.setAdapter(districtAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position>0){
                    Toast.makeText(SpinnerActivity.this, districts.get(position).toString(), Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}
