package com.bunhann.gazeettersample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bunhann.gazeettersample.adapter.DistrictRecyclerAdapter;
import com.bunhann.gazeettersample.adapter.ProvinceAdapter;
import com.bunhann.gazeettersample.model.District;
import com.bunhann.gazeettersample.model.Province;
import com.bunhann.gazeettersample.recycleenhancement.CustomItemDecorator;
import com.bunhann.gazeettersample.recycleenhancement.RecyclerTouchListener;
import com.bunhann.gazeettersample.repo.DistrictRepository;
import com.bunhann.gazeettersample.repo.ProvinceRepository;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.SlideInRightAnimator;

public class RecycleActivity extends AppCompatActivity {

    private Spinner provinceSpinner;
    private RecyclerView recyclerViewDistrict;

    private ArrayList<Province> provinces;
    private ArrayList<District> districts;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycle);

        provinceSpinner = (Spinner) findViewById(R.id.spinnerProvince);
        recyclerViewDistrict = (RecyclerView) findViewById(R.id.recycler_district);

        ProvinceRepository provinceRepository = new ProvinceRepository(this);
        provinces = new ArrayList<>();

        Province p0 = new Province();
        p0.setProCode(0);
        p0.setProvinceKh("សូមជ្រើសរើសមួយ");
        p0.setProvince("សូមជ្រើសរើសមួយ");
        provinces.add(p0);
        provinces.addAll(provinceRepository.getProvinceList());

        ProvinceAdapter provinceAdapter = new ProvinceAdapter(this, provinces);
        provinceSpinner.setAdapter(provinceAdapter);

        districts = new ArrayList<>();
        provinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DistrictRepository districtRepository = new DistrictRepository(view.getContext());
                districts = new ArrayList<>();
                DistrictRecyclerAdapter districtRecyclerAdapter;
                if (position>0){
                    districtRecyclerAdapter = new DistrictRecyclerAdapter(view.getContext(), districts);
                    districts.addAll(districtRepository.getDistrictByProvince(id));

                } else{
                    districts.clear();
                    districtRecyclerAdapter = new DistrictRecyclerAdapter(view.getContext(), districts);
                }
                recyclerViewDistrict.addItemDecoration(new CustomItemDecorator(view.getContext(), LinearLayoutManager.VERTICAL, 2));
                recyclerViewDistrict.setLayoutManager(new LinearLayoutManager(view.getContext()));
                recyclerViewDistrict.setItemAnimator(new SlideInRightAnimator());
                recyclerViewDistrict.setAdapter(districtRecyclerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        recyclerViewDistrict.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerViewDistrict, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Toast.makeText(RecycleActivity.this, districts.get(position).toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
}
